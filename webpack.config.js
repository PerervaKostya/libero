'use strict';

// const webpack = require("webpack");
// const JavaScriptObfuscator = require('webpack-obfuscator');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = [
  {
    entry: {
      public: "./src/public.jsx",
    },

    output: {
      path: __dirname + "/public/assets/",
      filename: '[name].js',
      library: "[name]"
    },

    watchOptions: {
      aggregateTimeout: 100
    },

    devtool: "source-map",

    resolve: {
      extensions: [".js", ".jsx"]
    },

    module: {
      rules: [
        {
          test: /\.jsx$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'react', 'es2015']
            }
          }
        }, {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
              {loader: "css-loader", options: {minimize: true}},
              {loader: "sass-loader"}
            ]
          })
        }
      ]
    },

    plugins: [
      new ExtractTextPlugin("css/main.css"),
    ]
  }
];
