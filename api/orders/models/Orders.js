'use strict';

/**
 * Lifecycle callbacks for the `Orders` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, result) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, results) => {},

  // Fired before a `fetch` operation.
  // beforeFetch: async (model) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, result) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  beforeCreate: async (model) => {
    var nodemailer = require('nodemailer');
    var Twig = require("twig");

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

    var transporter = nodemailer.createTransport({
      host: 'zcs.chicco.com.ua',
      port: 2525,
      secure: false,
      auth: {
        user: "order@babyshop.ua",
        pass: "ahlahSh7"
      }
    });

    Twig.renderFile(
      __dirname + '/../mail/mail.twig',
      model,
      (err, html) => {
        console.log(html);

        var mailOptions = {
          from: 'order@babyshop.ua',
          to: 'order@babyshop.ua',
          subject: 'Order maked',
          text: html
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        });
      }
    );
  },

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, result) => {},

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, result) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, result) => {}
};
