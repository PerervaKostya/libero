import React from "react";
import ReactDom from "react-dom";
import {Provider} from "react-redux";

import ProductList from "../products/ProductList";
import store from "../redux/Store";

/**
 *
 */
export default class ProductListHolder {
  /**
   *
   * @param node
   */
  constructor(node) {
    this.node = node;
  }

  /**
   *
   */
  ready() {
    ReactDom.render(
      <Provider store={store}>
        <ProductList/>
      </Provider>,
      this.node
    )
  }
}
