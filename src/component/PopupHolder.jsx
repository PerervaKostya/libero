import React from "react";
import ReactDom from "react-dom";
import {Provider} from "react-redux";

import Popuper from "../popups/Popuper";
import store from "../redux/Store";

/**
 *
 */
export default class PopupHolder {
  /**
   *
   * @param node
   */
  constructor(node) {
    this.node = node;
  }

  /**
   *
   */
  ready() {
    ReactDom.render(
      <Provider store={store}>
        <Popuper/>
      </Provider>,
      this.node
    )
  }
}
