import store from "../redux/Store";

/**
 *
 */
export default class RulesButton {
  /**
   *
   * @param node
   */
  constructor(node) {
    this.node = node;
  }

  /**
   *
   */
  ready() {
    this.node.onclick = () => store.dispatch({
      type: "openPopup",
      component: "RulesPopup"
    })
  }
}
