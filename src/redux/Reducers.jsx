import {createStore, combineReducers} from 'redux';
import store from "./Store";

import serialize from "serialize-for-xhr";
/**
 *
 */
class ReducerHolder {

  static init(state, action) {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', '/products/', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 3) {
        store.dispatch({
          type: "setProduct",
          data: this.responseText
        });
      }
    }

    xhr.send();
    return {
      products: {},
      popup: {}
    };
  }

  /**
   *
   * @param state
   * @param action
   */
  static setProduct(state, action) {
    const newState = Object.assign({}, state);
    let products = JSON.parse(action.data);
    let list = {};

    for (let i = 0; i < products.length; i++) {
      list[products[i].id] = products[i];
    }

    newState.products = list;
    return newState
  }

  /**
   *
   * @param state
   * @param action
   * @returns {{} & any}
   */
  static openPopup(state, action) {
    const newState = Object.assign({}, state);
    newState.popup.component = action.component;
    newState.popup.params = action.params;
    document.body.style.overflow = "hidden";
    return newState
  }

  /**
   *
   * @param state
   * @param action
   * @returns {{} & any}
   */
  static closePopup(state) {
    const newState = Object.assign({}, state);
    newState.popup.component = null;
    newState.popup.params = [];
    document.body.style.overflow = "";
    return newState
  }


  static sendForm(state, action) {
    let newState = Object.assign({}, state);

    var xhr = new XMLHttpRequest();

    xhr.open('POST', '/orders/', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 3) {

      }
    }

    xhr.send(serialize(action.params));

    newState = ReducerHolder.closePopup(state);

    return newState
  }
}
/**
 *
 * @param state
 * @param action
 * @returns {*}
 */
const reducer = (state, action) => {
  if (ReducerHolder[action.type]) {
    return ReducerHolder[action.type](state, action);
  } else {
    return state;
  }
};

export default reducer;
