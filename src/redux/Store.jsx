import Reducers from "./Reducers";
import {createStore} from "redux";

let store = createStore(Reducers);

export default store;