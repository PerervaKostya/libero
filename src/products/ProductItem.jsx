import React from "react";
import {connect} from "react-redux";

/**
 *
 */
class ProductItem extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    componentDidMount () {
        if (window.innerWidth < 1366) {
            var liberoTouch = document.querySelectorAll('.libero-touch');

            Array.prototype.forEach.call(liberoTouch, function (el) {
                el.classList.remove('t-animate');
            });
        }
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let item = this.props.products[this.props.id];

        return (
          <div className="t776__col t-col t-col_6 t-align_center t-item t776__col_mobile-grid js-product t-animate libero-touch"
               data-animate-style="zoomin"
               data-animate-chain="yes"
               data-product-lid="1522922212783"
               data-open-popup="order">

            <div className="t776__content">
              <div className="t776__imgwrapper t776__imgwrapper_mobile-nopadding"
                   onClick={this.buyProduct.bind(this)}
                   style={{paddingBbottom:67.857142857143}}>
                <img
                  src={item.image[0].url}
                  data-original={item.image[0].url}
                  className="t776__img t776__img_first_hover t-img js-product-img"
                  imgfield="li_gallery__1522922212783:::0"/>
                <img
                  src={item.image[1].url}
                  data-original={item.image[1].url}
                  className="t776__img t776__img_second t-img"/>
              </div>

              <div className="t776__textwrapper">
                <div className="t776__title t-name t-name_xl js-product-name"
                     field="li_title__1522922212783"
                     style={{color: "#5b1e83", fontSize: "30px", fontWeight:700}}>
                  {item.name}
                </div>

                <div className="t776__descr t-descr t-descr_xxs"
                     field="li_descr__1522922212783"
                     style={{color:"#5b1e83", fontSize: "18px", fontWeight:700}}>
                  {item.weight}
                </div>

                <div className="t776__price-wrapper">
                  <div className="t776__price t776__price-item t-name t-name_xs"
                       style={{color: "#929ff3", fontSize: "24px", fontWeight:700}}>
                    <div className="t776__price-value js-product-price"
                         field="li_price__1522922212783">
                      {item.price}
                    </div>

                    <div className="t776__price-currency">грн.</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }

    buyProduct() {
      this.props.dispatch({
          type: "openPopup",
          component: "BuyPopup",
          params: {
            id: this.props.id
          }
      })
    }
}

const mapStateToProps = store => {
  return {
    products: store.products
  };
};

export default connect(mapStateToProps)(ProductItem);
