import React from "react";
import {connect} from 'react-redux';

import ProductItem from "./ProductItem";

/**
 *
 */
class ProductList extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        if (!this.props.products) {
          return null;
        }

        let list = Object.keys(this.props.products).map(key => <ProductItem key={key} id={key}/>);

        return (
            <div className="t-container t776__container_mobile-grid">
                {list}
            </div>
        )
    }
}

const mapStateToProps = store => {
  return {
    products: store.products
  };
};

export default connect(mapStateToProps)(ProductList);
