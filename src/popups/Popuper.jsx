import React from "react";
import {connect} from "react-redux";
import {TransitionGroup, CSSTransition} from "react-transition-group";

/**
 *
 */
class Popuper extends React.Component {

  /**
   *
   * @param props
   */
  constructor(props) {
    super(props);
  }

  /**
   *
   * @returns {XML}
   */
  render() {
    return (
      <TransitionGroup component={null}>
        {this.renderPopup()}
      </TransitionGroup>
    )
  }

  /**
   *
   * @returns {*}
   */
  renderPopup() {
    if (this.props.component) {
      let Component = require("./" + this.props.component).default;

      return (
        <CSSTransition
          key={this.props.component}
          in={true}
          timeout={500}
          classNames="popup">
          <div className="popup">
            <span className="popup__substrate" onClick={this.closePopup.bind(this)}/>
            <Component {...this.props.params}/>
          </div>
        </CSSTransition>
      )
    } else {
      return null;
    }
  }

  /**
   *
   */
  closePopup() {
    this.props.dispatch({
      type: "closePopup"
    })
  }
}

/**
 *
 * @param store
 * @returns {{component, params}}
 */
const mapStateToProps = store => {
  return {
    component: store.popup.component,
    params: store.popup.params
  };
};

export default connect(mapStateToProps)(Popuper);
