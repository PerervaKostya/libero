import React from "react";
import {connect} from "react-redux";
/**
 *
 */
class RulesPopup extends React.Component {

  /**
   *
   * @param props
   */
  constructor(props) {
      super(props);
  }

  componentDidMount() {
    $(this.wrap).mCustomScrollbar({
      autoDraggerLength: false
    });
  }

  /**
   *
   * @returns {XML}
   */
  render() {
      return (
        <div className="popup-content popup-content_rules">
          {this.renderClose()}

          <div className="popup-content__title popup-title">
            <span className="popup-title__text">Официальные правила:</span>
          </div>

          <div className="popup-content__hold popup-content__hold_rules">
            {this.renderRules()}
          </div>
        </div>
      )
  }

  /**
  *
  * @returns {*}
  */
  renderClose() {
    return (
      <span className="popup__close" data-close="popup" onClick={this.closePopup.bind(this)}>
          <svg className="popup-content__close-icn" width="23px" height="23px" viewBox="0 0 23 23">
              <g stroke="none" strokeWidth="1" fill="#fff"> <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"/>
                  <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"/>
              </g>
          </svg>
      </span>
    )
  }

  /**
  *
  * @returns {*}
  */
  renderRules() {
    return (
      <div className="popup-content__wrapper" ref={wrap => this.wrap = wrap}>
        <p className="popup-content__text">
            Акція діє з 15.05.2018 до 31.08.2018
        </p>
        <p className="popup-content__text">
            Акція діє на території України у фірмових та інтернет магазинах Chicco. Актуальні адреси фірмових магазинів можна знайти на сайті <a href="http://chicco.com.ua" className="popup-content__link">chicco.com.ua</a>
        </p>
        <p className="popup-content__text">
            При першій покупці підгузків Libero Touch №1 покупець має право разово отримати набір пробників для мам та подарункових промокодів номіналом 300 грн. При наступній покупці підгузків Libero Touch №1 той же покупець має право отримати подарунковий промокод номіналом 200 грн.
        </p>
        <p className="popup-content__text">
            При кожній покупці підгузків Libero Touch №2 покупець має право разово отримати подарунковий промокод номіналом 200 грн.
        </p>
        <p className="popup-content__text">
            Подарункові промокоди номіналом 200 грн та 300 грн діють при покупці від 1000 грн. Знижки за дисконтною картою не діють.
        </p>
        <p className="popup-content__text">
            Промокод діє в інтернет-магазині chicco.com.ua. Промокод може бути використаний лише один раз.
        </p>
        <p className="popup-content__text">
            Промокод не може бути замінений на грошовий еквівалент. В одному чеку можна скористатися лише одним промокодом.
        </p>
        <p className="popup-content__text">
            У випадку, якщо номінал промокоду перевищує вартість товару, різниця за таким промокодом не повертається. У випадку, якщо номінал промокоду є нижчим від вартості товару, останній підлягає доплаті.
        </p>
        <p className="popup-content__text">
            У разі повернення акційного товару, промокод, виданий покупцю, анулюється. У разі, якщо промокод за певних обставин не був використаний (загублений та/або пошкоджений тощо), а товар визнається придбаним за повну вартість, при поверненні такого товару, покупцеві повертається вартість товару з вирахуванням знижки, що є еквівалентом невикористаного промокоду. <em>Наприклад: клієнт придбав товар на суму 3000 грн. та отримав промокод номіналом 500 грн., але не використав його (загубив та/або пошкодив тощо). При поверненні товару, клієнт у фірмовому магазині повинен отримати 2500 грн. (вартість покупки мінус сума промокоду).</em>
        </p>
        <p className="popup-content__text">
            Промокод діє до 31.08.2018 р.
        </p>
      </div>
    )
  }

  /**
   *
   */
  closePopup() {
      this.props.dispatch({
          type: "closePopup"
      })
  }
}
export default connect()(RulesPopup);