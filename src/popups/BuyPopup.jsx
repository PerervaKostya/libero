import React from "react";
import {connect} from "react-redux";
import InputMask from "react-input-mask";

/**
 *
 */
class BuyPopup extends React.Component {

  /**
   *
   * @param props
   */
  constructor(props) {
    super(props);

    this.item = this.props.products[this.props.id];


    this.state = {
      count: 1,
      product: this.item.name,
      name: undefined,
      phone: undefined,
      email: undefined
    };

    this.changeField = this.changeField.bind(this);
    this.focusField = this.focusField.bind(this);
    this.blurField = this.blurField.bind(this);
  }

  /**
   *
   * @returns {XML}
   */
  render() {
    return (
      <div className="popup-content">
        {this.renderClose()}

        <div className="popup-content__title popup-title">
          <span className="popup-title__text">Ваш заказ:</span>
        </div>

        <div className="popup-content__hold">
          <form className="form-order">
            <div className="form-order__wrapper">

              <ul className="form-order-card-list">
                {this.renderProduct()}
              </ul>

              <div className="form-order-card-total">
                  <span className="form-order-card-total__sum">
                    Сумма: {this.item.price * this.state.count} грн
                  </span>
              </div>
            </div>

            {this.renderForm()}
          </form>
        </div>
      </div>
    )
  }

  /**
   *
   * @returns {*}
   */
  renderClose() {
    return (
      <span className="popup__close" onClick={this.closePopup.bind(this)}>
          <svg className="popup-content__close-icn" width="23px" height="23px" viewBox="0 0 23 23" version="1.0">
              <g stroke="none" fill="#fff"> <rect
                transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                x="10.3137085" y="-3.6862915" width="2" height="30"/>
                  <rect
                    transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                    x="10.3137085" y="-3.6862915" width="2" height="30"/>
              </g>
          </svg>
      </span>
    )
  }

  /**
   *
   * @returns {*}
   */
  renderProduct() {
    return (
      <li className="form-order-card-list__item">
        <div className="form-order-card">
          <div className="form-order-card__row">
            <div className="form-order-card-left">
              <div className="form-order-card-left__wrap-img">
                <img src={this.item.image[0].url}
                     alt="goods"
                     className="form-order-card-left__img"/>
              </div>

              <div className="form-order-card-left__title">
                <span className="form-order-card-left__title-text">
                  {this.item.name}
                </span>
              </div>
            </div>

            <div className="form-order-card-right">
              {this.renderCount()}

              <span className="form-order-card__price">
                  {this.item.price} грн
              </span>

              <span className="form-order-card__clear" onClick={this.closePopup.bind(this)}>
                  <svg width="11.5px" height="11.5px">
                      <path stroke="rgb(84, 37, 133)" strokeWidth="1px" strokeLinecap="butt" strokeLinejoin="miter"
                            fill="rgb(84, 37, 133)"
                            d="M10.214,9.036 L9.036,10.214 L5.500,6.679 L1.964,10.214 L0.786,9.036 L4.321,5.500 L0.786,1.964 L1.964,0.786 L5.500,4.321 L9.036,0.786 L10.214,1.964 L6.679,5.500 L10.214,9.036 Z"/>
                  </svg>
              </span>
            </div>
          </div>
        </div>
      </li>
    )
  }

  /**
   *
   * @returns {*}
   */
  renderCount() {
    return (
      <div className="form-order-card-amount">
        <span className="form-order-card-amount__minus" onClick={this.decrease.bind(this)}>
            <svg className="form-order-card-amount__minus-ico"
                 width="10"
                 height="2">
                <path stroke="rgb(255, 255, 255)" strokeWidth="1px" strokeLinecap="butt" strokeLinejoin="miter"
                      fill="#ffffff" d="M0.656,0.656 L12.344,0.656 L12.344,2.344 L0.656,2.344 L0.656,0.656 Z"/>
            </svg>
        </span>

        <input type="text"
               className="form-order-card-amount__field"
               value={this.state.count}
               readOnly={true}/>

        <span className="form-order-card-amount__plus" onClick={this.increase.bind(this)}>
            <span className="form-order-card-amount-form__plus-ico">+</span>
        </span>
      </div>
    )
  }

  /**
   *
   */
  increase() {
    this.setState({
      count: this.state.count + 1
    })
  }

  /**
   *
   */
  decrease() {
    if (this.state.count > 1) {
      this.setState({
        count: this.state.count - 1
      })
    }
  }

  /**
   *
   * @returns {*}
   */
  renderForm() {
    return (
      <div className="form-order-user-data">
        <input id={"product"} value={this.item.name} type={"hidden"} readOnly={true}/>

        <div className="form-order-user-data__row">
          <input type="text"
                 value={this.state.name}
                 id="name"
                 onChange={this.changeField}
                 onFocus={this.focusField}
                 onBlur={this.blurField}
                 className="form-order-user-data__field"/>

          <label htmlFor="name" className="form-order-user-data__label">
            <span className="form-order-user-data__label-text">
              Введите имя
            </span>

            {this.renderError("name")}
          </label>
        </div>

        <div className="form-order-user-data__row">
          <InputMask
            type="text"
            id="phone"
            className="form-order-user-data__field"
            value={this.state.phone}
            onChange={this.changeField}
            onFocus={this.focusField}
            onBlur={this.blurField}
            mask={"+38 (099) 999 99 99"}
          />

          <label htmlFor="phone" className="form-order-user-data__label">
            <span className="form-order-user-data__label-text">
              Введите телефон
            </span>

            {this.renderError("phone")}
          </label>
        </div>

        <div className="form-order-user-data__row">
          <input type="text"
                 id="email"
                 className="form-order-user-data__field"
                 value={this.state.email}
                 onChange={this.changeField}
                 onFocus={this.focusField}
                 onBlur={this.blurField}
          />

          <label htmlFor="email" className="form-order-user-data__label">
            <span className="form-order-user-data__label-text">
              Введите email
            </span>

            {this.renderError("email")}
          </label>
        </div>

        <div className="form-order-bottom-wrap" onClick={this.sendForm.bind(this)}>
          <div className="form-order-submit">
            <span className="form-order-submit__text">
              Оформить
            </span>
          </div>
        </div>
      </div>
    )
  }

  /**
   *
   * @param field
   * @returns {*}
   */
  renderError(field) {
    if (this.state[field] === "") {
      return (
        <span className="form-order-user-data__error">
          некоректный ввод
        </span>
      )
    }
  }

  /**
   *
   * @param event
   */
  changeField(event) {
    let newState = this.state;
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  /**
   *
   * @param event
   */
  focusField(event) {
    let label = event.target.labels[0];
    label.classList.add('form-order-user-data__label_active');
  }

  /**
   *
   * @param event
   */
  blurField(event) {
    if (event.target.value.trim() == '') {
      let label = event.target.labels[0];
      label.classList.remove('form-order-user-data__label_active');

      let newState = this.state;
      newState[event.target.id] = "";
      this.setState(newState);
    }
  }

  /**
   *
   */
  sendForm() {
    let valid = Object.keys(this.state).filter(item => !this.state[item]);

    if (!/^[\+\d\(\) ]{19}$/.exec(this.state.phone)) {
      valid.push("phone");
    }

    if (valid.length) {
      this.checkValid(valid);
    } else {
      this.props.dispatch({
        type: "sendForm",
        params: this.state
      });
    }
  }

  /**
   *
   * @param valid
   */
  checkValid(valid) {
    let newState = this.state;

    valid.map(item => {
        newState[item] = ""
    });

    this.setState(newState);
  }

  /**
   *
   */
  closePopup() {
    this.props.dispatch({
      type: "closePopup"
    })
  }
}

/**
 *
 * @param store
 * @returns {{products: *|{}|products}}
 */
const mapStateToProps = store => {
  return {
    products: store.products
  };
};

export default connect(mapStateToProps)(BuyPopup);
