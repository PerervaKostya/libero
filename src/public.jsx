import React from "react";
import store from "./redux/Store";

import "../public/assets/scss/main.scss";
/**
 *
 */
class PublicHolder {

  /**
   *
   */
  constructor() {
    this.nodes = this.getComponentsList();
    this.componentReady = this.componentReady.bind(this);
    this.createComponent = this.createComponent.bind(this);

    this.initStore();
    this.initComponents();
  }

  /**
   *
   */
  initStore() {
      store.dispatch({
        type: "init"
      });
  }

  /**
   *
   * @retuwwebpackrns {NodeList}
   */
  getComponentsList() {
    return document.querySelectorAll("[data-component]");
  }

  /**
   *
   */
  initComponents() {
    this.components = Object.keys(this.nodes).map(this.createComponent);

    document.addEventListener(
      "DOMContentLoaded",
      () => Object.keys(this.components).map(this.componentReady)
    );
  }

  /**
   *
   * @param key
   */
  componentReady(key) {
    let component = this.components[key];

    if (screen.width > 768) {
      if (component.ready) {
        component.ready();
      }
    } else {
      if (component.mobileReady) {
        component.mobileReady();
      } else if (component.ready) {
        component.ready();
      }
    }
  }

  /**
   *
   * @param key
   */
  createComponent(key) {
    let node = this.nodes[key];
    let componentName = node.dataset.component;
    let Component = require("./component/" + componentName).default;

    return new Component(node);
  }
}

new PublicHolder;
